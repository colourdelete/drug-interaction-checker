package drugCheck

import (
	"encoding/json"
	"flag"
	"fmt"
	"gitlab.com/colourdelete/drug-interaction-checker/pkg/checker"
	"io/ioutil"
)

func PanicNonNilError(err error) {
	if err != nil {
		panic(err)
	}
}

func Run() {
	var inPathFlag = flag.String("in-path", "", "path of input JSON file. eg ./input.json")
	var outPathFlag = flag.String("out-path", "", "path of output JSON file. eg ./result.json")
	flag.Parse()
	if *inPathFlag == "" {
		*inPathFlag = "./input.json"
	}
	if *outPathFlag == "" {
		*outPathFlag = "./output.json"
	}

	drugNames, err := GetDrugNamesFromPath(*inPathFlag)
	PanicNonNilError(err)
	rxCUIs := make([]checker.RxCUI, 0)

	for _, drugName := range drugNames {
		rxCUI, err := checker.GetRxCUIFromName(drugName)
		PanicNonNilError(err)
		rxCUIs = append(rxCUIs, rxCUI)
	}

	//rxCUIs = []checker.RxCUI{
	//	"207106",
	//	"152923",
	//}
	rawInters, err := checker.GetRawInteractionsFromRxCUIs(rxCUIs)

	PanicNonNilError(err)

	inters := checker.GetInteractionsFromRawInteractions(rawInters)
	//inters = checker.FillInteractionsWithNamesRxCUIs(inters, drugNames, rxCUIs)
	simpleInters := checker.GetSimpleInteractionsFromInteractions(inters)

	PanicNonNilError(err)

	fmted, err := json.Marshal(simpleInters)

	PanicNonNilError(err)

	err = ioutil.WriteFile(*outPathFlag, fmted, 0644)

	PanicNonNilError(err)

	err = ioutil.WriteFile(fmt.Sprintf("%s.txt", *outPathFlag), []byte(simpleInters.Format()), 0644)

	PanicNonNilError(err)
}

func GetDrugNamesFromPath(path string) ([]string, error) {
	var drugNames []string
	src, err := ioutil.ReadFile(path)
	if err != nil {
		return nil, err
	}
	err = json.Unmarshal(src, &drugNames)
	if err != nil {
		return nil, err
	}
	return drugNames, nil
}
