package checker

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"net/url"
)

func GetRxCUIFromName(name string) (RxCUI, error) {
	BaseURL := "https://rxnav.nlm.nih.gov/REST/rxcui.json?name=%s&search=1"
	callURL := fmt.Sprintf(
		BaseURL,
		url.QueryEscape(name),
	)

	resp, err := http.Get(callURL)
	if err != nil {
		return "", err
	}

	defer func() {
		err := resp.Body.Close()
		if err != nil {
			panic(err)
		}
	}()

	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return "", err
	}

	respData := RxCUIFromNameResponse{}
	err = json.Unmarshal(body, &respData)
	if err != nil {
		return "", err
	}

	if len(respData.IDGroup.RxCUIs) < 1 {
		return "", nil // nonexistent
	}
	return RxCUI(respData.IDGroup.RxCUIs[0]), nil
}

func GetRawInteractionsFromRxCUIs(RxCUIs []RxCUI) (RawInteractions, error) {
	BaseURL := "https://rxnav.nlm.nih.gov/REST/interaction/list.json?rxcuis=%s"
	RxCUIsFmted := ""
	for i, RxCUI := range RxCUIs {
		RxCUIsFmted += url.QueryEscape(string(RxCUI))
		if i != len(RxCUIs)-1 {
			RxCUIsFmted += "+"
		}
	}
	callURL := fmt.Sprintf(
		BaseURL,
		RxCUIsFmted,
	)

	resp, err := http.Get(callURL)
	if err != nil {
		return RawInteractions{}, err
	}

	defer func() {
		err := resp.Body.Close()
		if err != nil {
			panic(err)
		}
	}()

	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return RawInteractions{}, err
	}

	respData := RawInteractions{}
	err = json.Unmarshal(body, &respData)
	if err != nil {
		return RawInteractions{}, err
	}
	return respData, nil
}

func GetDrugsFromInteractionConcept(src InteractionConcept) []Drug {
	var drugs []Drug
	for _, item := range src {
		drugs = append(
			drugs,
			Drug{
				RxCUI: item.MinConceptItem.RxCUI,
				Name:  item.MinConceptItem.Name,
				Tty:   item.MinConceptItem.Tty,
				ID:    item.SourceConceptItem.ID,
				URL:   item.SourceConceptItem.URL,
			},
		)
	}
	return drugs
}

func GetInteractionsFromRawInteractions(src RawInteractions) Interactions {
	dest := Interactions{
		Disclaimer:   src.NlmDisclaimer,
		Interactions: []Interaction{},
	}
	for _, group := range src.FullInteractionTypeGroup {
		for _, v := range group.FullInteractionType {
			for _, pair := range v.InteractionPair {
				interaction := Interaction{
					Source: Source{
						Name:       group.SourceName,
						Disclaimer: group.SourceDisclaimer,
					},
					Comment: v.Comment,
					Group: Group{
						Drugs:    GetDrugsFromInteractionConcept(pair.InteractionConcept),
						Severity: pair.Severity,
						Comment:  pair.Description,
					},
				}
				dest.Interactions = append(dest.Interactions, interaction)
			}
		}
	}
	return dest
}

func FillInteractionsWithNamesRxCUIs(inters Interactions, names []string, rxCUIs []RxCUI) Interactions {
	for _, inter := range inters.Interactions {
		for _, drugName := range inter.Group.Drugs {
			for i, rxCUI := range rxCUIs {
				if drugName.RxCUI == rxCUI {
					copy(names[i:], names[i+1:])
					names[len(names)-1] = ""
					names = names[:len(names)-1]
					copy(rxCUIs[i:], rxCUIs[i+1:])
					rxCUIs[len(rxCUIs)-1] = ""
					rxCUIs = rxCUIs[:len(rxCUIs)-1]
				}
			}
		}
	}
	drugs := make([]Drug, 0)
	for i, rxCUI := range rxCUIs {
		name := names[i]
		drugs = append(
			drugs,
			Drug{
				RxCUI: rxCUI,
				Name:  name,
				Tty:   "unknown",
				ID:    "unknown",
				URL:   "unknown",
			},
		)
	}
	inters.Interactions = append(
		inters.Interactions,
		Interaction{
			Source: Source{
				Name:       "NIH",
				Disclaimer: "It is not the intention of NLM to provide specific medical advice, but rather to provide users with information to better understand their health and their medications. NLM urges you to consult with a qualified physician for advice about medications.",
			},
			Comment: "These drugs do not seem to have any recorded interactions. Consult with a qualified physician for advice about medications.",
			Group: Group{
				Drugs:    drugs,
				Severity: "unknown",
				Comment:  "unknown",
			},
		},
	)
	return inters
}

func GetSimpleInteractionsFromInteractions(interactions Interactions) SimpleInteractions {
	simpleInters := SimpleInteractions{}
	for _, inter := range interactions.Interactions {
		simpleDrugs := []string{}
		for _, simpleDrug := range inter.Group.Drugs {
			simpleDrugs = append(
				simpleDrugs,
				fmt.Sprintf(
					"%s (RxCUI: %s, URL: %s)",
					simpleDrug.Name,
					simpleDrug.RxCUI,
					simpleDrug.URL,
				),
			)
		}
		simpleInters = append(
			simpleInters,
			SimpleInteraction{
				Drugs: simpleDrugs,
				Description: fmt.Sprintf(
					"%s\n\n%s\n\n%s\n\n%s",
					interactions.Disclaimer,
					inter.Source.Disclaimer,
					inter.Comment,
					inter.Group.Comment),
				Source: inter.Source.Name,
			},
		)
	}
	return simpleInters
}
